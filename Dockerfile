FROM ubuntu:latest
MAINTAINER Denis Grigoryev <dgrigoryev@maprox.net>
RUN \
    apt-get update && \
    apt-get -y install npm python nodejs wget libglu1-mesa libqt5webkit5-dev libcairo2-dev libjpeg8-dev libpango1.0-dev libgif-dev build-essential g++ && \
    ln -s /usr/bin/nodejs /usr/bin/node && \
    npm install gitbook-cli -g
RUN wget -nv -O- https://raw.githubusercontent.com/kovidgoyal/calibre/master/setup/linux-installer.py | python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"
