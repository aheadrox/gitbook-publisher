# GitBook docker container

Use with gitlab-ci:

```yaml

image: registry.gitlab.com/aheadrox/gitbook-publisher:master-latest

pages:
  script:
    - gitbook build ./ ./public
    - gitbook pdf ./ ./public/general.pdf
  artifacts:
    paths:
    - public
  only:
    - develop
    - master

```
